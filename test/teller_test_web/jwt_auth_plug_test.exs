defmodule TellerTestWeb.JwtAuthPlugTest do
  use TellerTestWeb.ConnCase

  test "request is denied when authorizaytion fails" do
    conn =
      build_conn()
      |> Plug.Conn.put_req_header("authorization", "Basic nope")
      |> TellerTestWeb.JwtAuthPlug.call(%{})

    assert conn.status == 401
  end

  test "request passes through when authorization succeeds" do
    conn =
      build_conn()
      |> Plug.Conn.put_req_header("authorization", "Basic #{generate_valid_token()}")
      |> TellerTestWeb.JwtAuthPlug.call(%{})

    assert conn.status != 401
  end

  defp generate_valid_token() do
    token = TellerTestWeb.JwtAuthToken.generate_and_sign!(%{seed: 1234})

    "test_"
    |> Kernel.<>(token)
    |> Kernel.<>(":")
    |> Base.encode64()
  end
end
