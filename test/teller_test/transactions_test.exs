defmodule TellerTest.TransactionsTest do
  use ExUnit.Case

  alias TellerTest.Transactions

  describe "get_transactions_for_account_id/1" do
    test "returns transactions for last 90 days for account" do
      account_id = "test_acc_51340"

      transactions = Transactions.get_transactions_for_account_id(account_id)
      first_txn = hd(transactions)
      last_txn = List.last(transactions)

      assert length(transactions) == 90
      assert Date.compare(first_txn.date, Date.utc_today()) == :eq
      assert first_txn.account_id == account_id
      assert Date.compare(last_txn.date, Date.add(Date.utc_today(), -89)) == :eq
      assert last_txn.account_id == account_id
    end
  end

  describe "get_transaction/2" do
    test "returns transaction for given id" do
      account_id = "test_acc_51340"
      id = "test_txn_2020072551340"

      txn = Transactions.get_transaction(id, account_id)

      assert txn.id == id
      assert txn.account_id == account_id
      assert txn.running_balance == 95.63
    end
  end
end
