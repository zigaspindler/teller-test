defmodule TellerTest.AccountsTest do
  use ExUnit.Case

  alias TellerTest.Accounts

  describe "get_accounts_for_seed/1" do
    test "returns 3 accounts" do
      assert [acc_1, acc_2, acc_3] = Accounts.get_accounts_for_seed(1234)

      assert acc_1.name == "Jimmy Carter"
      assert acc_1.id == "test_acc_51340"

      assert acc_2.name == "George W. Bush"
      assert acc_2.id == "test_acc_22414"

      assert acc_3.name == "George H. W. Bush"
      assert acc_3.id == "test_acc_80340"
    end
  end

  describe "get_account/1" do
    test "returns account with given id" do
      id = "test_acc_51340"

      account = Accounts.get_account(id)

      assert account.id == id
      assert account.name == "Jimmy Carter"
    end
  end
end
