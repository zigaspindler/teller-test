defmodule TellerTestWeb.TransactionController do
  use TellerTestWeb, :controller

  alias TellerTest.Transactions

  def index(conn, %{"account_id" => account_id}) do
    transactions = Transactions.get_transactions_for_account_id(account_id)

    render(conn, "index.json", transactions: transactions)
  end

  def show(conn, %{"id" => id, "account_id" => account_id}) do
    transaction = Transactions.get_transaction(id, account_id)

    render(conn, "show.json", transaction: transaction)
  end
end
