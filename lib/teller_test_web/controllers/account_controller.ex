defmodule TellerTestWeb.AccountController do
  use TellerTestWeb, :controller

  alias TellerTest.Accounts

  def index(conn, _params) do
    accounts = Accounts.get_accounts_for_seed(conn.assigns.seed)

    render(conn, "index.json", accounts: accounts)
  end

  def show(conn, %{"id" => id}) do
    account = Accounts.get_account(id)

    render(conn, "show.json", account: account)
  end
end
