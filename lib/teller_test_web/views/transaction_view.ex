defmodule TellerTestWeb.TransactionView do
  use TellerTestWeb, :view

  def render("index.json", %{transactions: transactions}) do
    render_many(transactions, __MODULE__, "transaction.json")
  end

  def render("show.json", %{transaction: transaction}) do
    render_one(transaction, __MODULE__, "transaction.json")
  end

  def render("transaction.json", %{transaction: transaction}) do
    %{
      id: transaction.id,
      type: transaction.type,
      date: transaction.date,
      account_id: transaction.account_id,
      links: create_links(transaction),
      amount: transaction.amount,
      description: transaction.description,
      running_balance: Float.floor(transaction.running_balance, 2)
    }
  end

  defp create_links(%{id: id, account_id: account_id}) do
    %{
      self: Routes.account_transaction_url(TellerTestWeb.Endpoint, :show, account_id, id),
      account: Routes.account_url(TellerTestWeb.Endpoint, :show, account_id)
    }
  end
end
