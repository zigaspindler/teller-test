defmodule TellerTestWeb.AccountView do
  use TellerTestWeb, :view

  def render("index.json", %{accounts: accounts}) do
    render_many(accounts, __MODULE__, "account.json")
  end

  def render("show.json", %{account: account}) do
    render_one(account, __MODULE__, "account.json")
  end

  def render("account.json", %{account: account}) do
    %{
      id: account.id,
      name: account.name,
      enrollment_id: account.enrollment_id,
      institution: account.institution,
      currency_code: account.currency_code,
      account_number: account.account_number,
      balances: account.balances,
      links: create_links(account)
    }
  end

  defp create_links(%{id: id}) do
    %{
      self: Routes.account_url(TellerTestWeb.Endpoint, :show, id),
      transactions: Routes.account_transaction_url(TellerTestWeb.Endpoint, :index, id)
    }
  end
end
