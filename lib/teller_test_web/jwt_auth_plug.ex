defmodule TellerTestWeb.JwtAuthPlug do
  import Plug.Conn

  alias TellerTestWeb.JwtAuthToken

  def init(opts), do: opts

  def call(conn, _opts) do
    case get_req_header(conn, "authorization") do
      ["Basic " <> attempted_auth] -> verify(conn, attempted_auth)
      _ -> unauthorized(conn)
    end
  end

  defp verify(conn, auth) do
    auth
    |> Base.decode64!()
    |> String.split(":")
    |> List.first()
    |> String.slice(5..-1)
    |> JwtAuthToken.verify_and_validate()
    |> case do
      {:ok, claims} ->
        {:ok, seed} = Map.fetch(claims, "seed")

        assign(conn, :seed, seed)

      _ ->
        unauthorized(conn)
    end
  end

  defp unauthorized(conn) do
    conn
    |> send_resp(
      401,
      Jason.encode!(%{
        error: %{
          code: 401,
          message: "A request was made without an access token where one was required."
        }
      })
    )
    |> halt()
  end
end
