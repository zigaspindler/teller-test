defmodule TellerTest.Accounts do
  alias TellerTest.Accounts.Account

  def get_accounts_for_seed(seed) do
    :rand.seed(:exsss, seed)

    account_ids =
      for _i <- 0..2 do
        Enum.random(10000..99999)
      end

    account_ids
    |> Enum.map(fn id ->
      Account.generate_account(id)
    end)
  end

  def get_account(id) do
    id
    |> parse_seed_from()
    |> Account.generate_account()
  end

  defp parse_seed_from("test_acc_" <> seed_string) do
    {seed, _} = Integer.parse(seed_string)

    seed
  end
end
