defmodule TellerTest.Accounts.Account do
  defstruct [
    :account_number,
    :balances,
    :enrollment_id,
    :id,
    :institution,
    :name,
    :routing_numbers,
    currency_code: "USD"
  ]

  @names [
    "Jimmy Carter",
    "Ronald Reagan",
    "George H. W. Bush",
    "Bill Clinton",
    "George W. Bush",
    "Barack Obama",
    "Donald Trump"
  ]
  @institutions ["Chase", "Bank of America", "Wells Fargo", "Citi", "Capital One"]

  def generate_account(seed) do
    :rand.seed(:exsss, seed)

    %__MODULE__{
      id: "test_acc_#{seed}",
      enrollment_id: "test_enr_#{seed}"
    }
    |> set_account_number()
    |> set_name()
    |> set_institution()
    |> set_balances()
  end

  defp set_account_number(account) do
    acc_number = Enum.random(1_000_000_000..9_999_999_999) |> Integer.to_string()

    Map.put(account, :account_number, acc_number)
  end

  defp set_name(account) do
    position = Enum.random(0..(length(@names) - 1))

    Map.put(account, :name, Enum.at(@names, position))
  end

  defp set_institution(account) do
    position = Enum.random(0..(length(@institutions) - 1))
    name = Enum.at(@institutions, position)

    id =
      name
      |> String.replace(" ", "_")
      |> String.downcase()

    Map.put(account, :institution, %{id: id, name: name})
  end

  defp set_balances(%{id: id} = account) do
    balance =
      id
      |> TellerTest.Transactions.get_current_balance_for_account_id()
      |> Float.floor(2)

    Map.put(account, :balances, %{available: balance, ledger: balance})
  end
end
