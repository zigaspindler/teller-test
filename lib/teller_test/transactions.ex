defmodule TellerTest.Transactions do
  alias TellerTest.Transactions.Transaction

  def get_transactions_for_account_id(account_id) do
    Date.utc_today()
    |> generate_transactions_until(account_id)
    |> Enum.take(90)
  end

  def get_transaction(id, account_id) do
    id
    |> get_date_from_id(account_id)
    |> generate_transactions_until(account_id)
    |> List.first()
  end

  def get_current_balance_for_account_id(account_id) do
    account_id
    |> get_transactions_for_account_id()
    |> List.first()
    |> Map.fetch!(:running_balance)
  end

  defp generate_transactions_until(date, account_id) do
    generate_transactions_until([], ~D[2020-04-01], date, account_id)
  end

  defp generate_transactions_until(transactions, current_date, end_date, account_id) do
    case Date.compare(current_date, end_date) do
      :gt ->
        transactions

      _ ->
        new_txn =
          Transaction.generate_transaction(
            current_date,
            account_id,
            current_running_balance(transactions)
          )

        generate_transactions_until(
          [new_txn | transactions],
          Date.add(current_date, 1),
          end_date,
          account_id
        )
    end
  end

  defp current_running_balance([txn | _]), do: txn.running_balance
  defp current_running_balance([]), do: 0.0

  defp get_date_from_id("test_txn_" <> id, "test_acc_" <> account_id) do
    raw_date = String.replace(id, account_id, "")

    {year, _} = Integer.parse(String.slice(raw_date, 0, 4))
    {month, _} = Integer.parse(String.slice(raw_date, 4, 2))
    {day, _} = Integer.parse(String.slice(raw_date, 6, 2))

    {:ok, date} = Date.new(year, month, day)

    date
  end
end
