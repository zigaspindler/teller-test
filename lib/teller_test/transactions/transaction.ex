defmodule TellerTest.Transactions.Transaction do
  defstruct [
    :running_balance,
    :id,
    :description,
    :date,
    :amount,
    :account_id,
    type: "card_payment"
  ]

  @merchants [
    "Uber",
    "Uber Eats",
    "Lyft",
    "Five Guys",
    "In-N-Out Burger",
    "Chick-Fil-A",
    "AMC Metreon",
    "Apple",
    "Amazon",
    "Walmart",
    "Target",
    "Hotel Tonight",
    "Misson Ceviche",
    "The Creamery",
    "Caltrain",
    "Wingstop",
    "Slim Chickens",
    "CVS",
    "Duane Reade",
    "Walgreens",
    "Rooster & Rice",
    "McDonald's",
    "Burger King",
    "KFC",
    "Popeye's",
    "Shake Shack",
    "Lowe's",
    "The Home Depot",
    "Costco",
    "Kroger",
    "iTunes",
    "Spotify",
    "Best Buy",
    "TJ Maxx",
    "Aldi",
    "Dollar General",
    "Macy's",
    "H.E. Butt",
    "Dollar Tree",
    "Verizon Wireless",
    "Sprint PCS",
    "T-Mobile",
    "Kohl's",
    "Starbucks",
    "7-Eleven",
    "AT&T Wireless",
    "Rite Aid",
    "Nordstrom",
    "Ross",
    "Gap",
    "Bed, Bath & Beyond",
    "J.C. Penney",
    "Subway",
    "O'Reilly",
    "Wendy's",
    "Dunkin' Donuts",
    "Petsmart",
    "Dick's Sporting Goods",
    "Sears",
    "Staples",
    "Domino's Pizza",
    "Pizza Hut",
    "Papa John's",
    "IKEA",
    "Office Depot",
    "Foot Locker",
    "Lids",
    "GameStop",
    "Sephora",
    "MAC",
    "Panera",
    "Williams-Sonoma",
    "Saks Fifth Avenue",
    "Chipotle Mexican Grill",
    "Exxon Mobil",
    "Neiman Marcus",
    "Jack In The Box",
    "Sonic",
    "Shell"
  ]

  def generate_transaction(date, "test_acc_" <> account_seed = account_id, running_balance) do
    {seed, _} =
      date
      |> Date.to_string()
      |> String.replace("-", "")
      |> Kernel.<>(account_seed)
      |> Integer.parse()

    :rand.seed(:exsss, seed)

    %__MODULE__{
      id: "test_txn_#{seed}",
      date: date,
      account_id: account_id
    }
    |> set_amount_and_balance(running_balance)
    |> set_description()
  end

  defp set_description(transaction) do
    position = Enum.random(0..(length(@merchants) - 1))

    Map.put(transaction, :description, Enum.at(@merchants, position))
  end

  defp set_amount_and_balance(transaction, running_balance) do
    sign = if :rand.uniform() > 0.5, do: 1.0, else: -1.0
    amount = ((:rand.uniform() + 0.1) * 10.0 * sign) |> Float.floor(2)

    %{transaction | amount: amount, running_balance: running_balance + amount}
  end
end
